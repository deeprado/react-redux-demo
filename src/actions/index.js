let nextTodoId = 0;
export const addTodo = (text) => ({
    type: 'ADD_TODO',
    id: nextTodoId++,
    text
});

export const setVisibilityFilter = (filter) => ({type: 'SET_VISIBILITY_FILTER', filter});

export const toggleTodo = (id) => ({type: 'TOGGLE_TODO', id});

export const deleteTodo = (id) => ({type: 'DELETE_TODO', id});

let nextUserId = 0;
export const createUser = (username='aaa',password='aaa') => ({
    type: 'ADD_USER',
    id : nextUserId ++,
    username,
    password,
    deleted: false
})

export const deleteUser = (id) => ({
    type: 'DELETE_USER',
    id
})

export const initUser = (users) => ({
    type: 'INIT_USER',
    users
})