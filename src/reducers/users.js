const user = (state, action) => {
    switch (action.type) {
        case 'ADD_USER': 
            return {
              id: action.id,
              username: action.username,
              password: action.password,
              deleted: action.deleted
            }
        default: 
            return state;
    }
}

const users = (state = [], action) => {
    switch (action.type) {
        case 'ADD_USER':
            return [...state, user(undefined, action)];
        case 'DELETE_USER':
            return state.filter(user => user.id !== action.id);
        case 'INIT_USER':
            action.users.map((user) => {
                state = [...state, user];
            })
            return state;
        default:
            return state
    }
};
export default users
