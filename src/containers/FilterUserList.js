

import {
    connect
} from 'react-redux';
import {
    deleteUser
} from '../actions/index';
import UserList from '../components/UserList';

const getVisibleUsers = (users, filter) => {
    switch (filter) {
        case 'SHOW_ALL':
            return users;
        case 'SHOW_NORMAL':
            return todos.filter(u => !u.deleted);
        case 'SHOW_DELETED':
            return todos.filter(u => u.deleted);
        default:
            throw new Error('Unknown filter: ' + filter)
    }
};

const mapStateToProps = (state) => ({
    users: getVisibleUsers(state.users, state.visibilityFilter)
});

const mapDispatchToProps = {
    delUserClick: deleteUser
};

const VisibleTodoList = connect(mapStateToProps, mapDispatchToProps)(UserList);

export default VisibleTodoList