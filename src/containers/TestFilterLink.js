
import TestLink from '../components/TestLink';

import {connect } from 'react-redux';
import {setVisibilityFilter} from '../actions/index';

const mapStateToProps = (state, ownProps) => ({
    active: ownProps.filter === state.visibilityFilter
});

const mapDispatchToProps =(dispatch, ownProps) => ({
    onClick: () => {
        dispatch(setVisibilityFilter(ownProps.filter))
    }
});


const TestFilterLink = connect(
    mapStateToProps,
    mapDispatchToProps
)(TestLink);

export default TestFilterLink
