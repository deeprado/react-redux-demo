

import React,{Component} from 'react';

import User from './User';

export default class extends Component{
    render(){
        const {users} = this.props
        return(
            <ul>
                {users.map(user => {
                    return (
                        <User
                            key={user.id}
                            {...user}
                            delClick={() => delUserClick(user.id)}
                        />)
                    }
                )}
            </ul>
        )
    }
}