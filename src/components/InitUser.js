/**
 * Created by 0easy-23 on 2017/8/15.
 */
import React, {Component} from 'react';
import {initUser} from '../actions/index';

var users = [
    {
        username: 'aaa',
        password: 'bbb',
        deleted: false
    },
    {
        username: 'aaaa',
        password: 'bbbb',
        deleted: false
    }
]
export default class extends Component {
    render() {
        const {dispatch} = this.props;
        return (
            <div className="inner">
                <form onSubmit={e=>{
                    e.preventDefault();
                    dispatch(initUser(users))
                }} >
                    <button type="submit">
                        Init User
                    </button>
                </form>
            </div>
        )
    }
}
