/**
 * Created by 0easy-23 on 2017/8/15.
 */
import React,{Component} from 'react';
import TestFilterLink from '../containers/TestFilterLink';
export default class extends Component{
   render(){
       return (
           <p className="footer">
               Show:{" "}
               <TestFilterLink filter="SHOW_ALL">
                   All
               </TestFilterLink>
               {" | "}
               <TestFilterLink filter="SHOW_ACTIVE">
                   Active
               </TestFilterLink>
               {" | "}
               <TestFilterLink filter="SHOW_COMPLETED">
                   Completed
               </TestFilterLink>
           </p>
       )
   }
}