import React,{Component} from 'react';

import AddTodo from '../containers/AddTodo';
import Footer from './Footer';
import TestFooter from './TestFooter';
import VisibleTodoList from '../containers/VisibleTodoList';

import AddUser from '../containers/AddUser';
import InitUser from '../containers/InitUser';
import FilterUserList from '../containers/FilterUserList';

export default class extends Component{
    render(){
        return(
            <div className="container">
                <AddTodo />
                <VisibleTodoList />
                <Footer/>
                <TestFooter/>
                <AddUser />
                <InitUser />
                <FilterUserList />
            </div>
        )
    }
}