/**
 * Created by 0easy-23 on 2017/8/15.
 */
import React, {Component} from 'react';
import {createUser} from '../actions/index';

export default class extends Component {
    render() {
        const {dispatch} = this.props;
        return (
            <div className="inner">
                <form onSubmit={e=>{
                    e.preventDefault();
                    dispatch(createUser(this.username, this.password))
                }} >
                    <input type="text" placeholder="React-Redux Todolist" ref={node => {
                        this.username = node
                    }}/>
                    <input type="text" placeholder="React-Redux Todolist" ref={node => {
                        this.password = node
                    }}/>
                    <button type="submit">
                        Add User
                    </button>
                </form>
            </div>
        )
    }
}
